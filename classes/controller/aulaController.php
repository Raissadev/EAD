<?php
	namespace controller;
	use \views\mainView;

	class aulaController
	{
		public function index($arg){
			//Validação de segurança
			if(isset($_SESSION['login_aluno']) == false){
				\Painel::redirect(INCLUDE_PATH);
			}else if(\models\homeModel::hasCursoById($_SESSION['id_aluno']) == false){
				\Painel::redirect(INCLUDE_PATH);
			}
			$idAula = $arg[3];
			$aula = \MySql::conectar()->prepare("SELECT * FROM `tb_admin.aulas` WHERE id = ?");
			$aula->execute(array($idAula));
			if($aula->rowCount() == 0){
				echo '<script>alert("A Aula não existe!");</script>';
				\Painel::redirect(INCLUDE_PATH);
			}else{
				$aula = $aula->fetch();
			mainView::render('area_aluno.php',$aula);
			}
		}
}
?>