<?php
	namespace controller;
	use \views\mainView;

	class homeController
	{
		public function home(){
			mainView::render('home.php');
		}
		public function index(){
			if(isset($_GET['addCurso'])){
				\MySql::conectar()->exec("INSERT INTO `tb_admin.curso_controle` VALUES (null,$_SESSION[id_aluno])");
				
				\Painel::redirect(INCLUDE_PATH);
			}
			if(isset($_GET['deslogar'])){
				unset($_SESSION['login_aluno']);

				\Painel::redirect(INCLUDE_PATH);
			}
			if(!isset($_SESSION['login_aluno'])){
				mainView::render('login.php');
			}else{
				mainView::render('area_aluno.php');
			}
		}
		public function login(){
			mainView::render('login.php');
	}
}
?>