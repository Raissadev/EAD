<?php 
    if(\models\homeModel::hasCursoById($_SESSION['id_aluno'])){
?>
<div class="separate"></div><!--separate-->
<section class="contentStudent sectionDefault">
    <div class="wrap">
        <div class="row itemsFlex alignCenter">
            <div class="contentClasses">
                <nav class="modules">
                    <ul>
                            <?php
                                $modulos = \MySql::conectar()->prepare("SELECT * FROM `tb_admin.modulos`");
                                $modulos->execute();
                                $modulos = $modulos->fetchAll();
                                foreach($modulos as $key => $value){
                            ?>
                        <li class="activeModule">
                            <button class="accordion"><a><?php echo $value['nome'] ?></a></button>
                            <ul class="subMenu panel">
                                    <?php
                                        $aulas = \MySql::conectar()->prepare("SELECT * FROM `tb_admin.aulas` WHERE modulo_id = $value[id]");
                                        $aulas->execute();
                                        $aulas = $aulas->fetchAll();
                                        foreach($aulas as $key => $aula){
                                    ?>
                                <li><i class="ri-eye-line"></i><a href="<?php echo INCLUDE_PATH ?>area_aluno/aula/<?php echo $aula['id'] ?>"><?php echo $aula['nome'] ?></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>
                    </ul>
                </nav><!--modules-->
            </div><!--contentClasses-->
            <div class="btnIframe">
                <a class="btnArrow btnAluno" id="btnAluno"><i class="ri-arrow-left-s-line"></i></a>
            </div><!--btnIframe-->
            <div class="contentIframe">
                <div class="iframe itemsFlex alignCenter justEnd">
                    <iframe class="transform" src="<?php echo $arr['link_aula'] ?>"></iframe>
                </div><!--iframe-->
            </div><!--contentIframe-->
        </div><!--row-->
    </div><!--wrap-->
</section><!--contentStudent-->

<section class="infoUser">
    <div class="wrap w100">
        <div class="row">
            <div class="cardUser">
                <div class="headerCardUser">
                    <figure class="imgCardUser itemsFlex alignCenter justCenter marginDownSmall">
                        <img src="<?php echo INCLUDE_PATH ?>images/myWallpaper.jpg" />
                    </figure><!--imgCardUser-->
                    <div class="infoUser textCenter">
                        <h2><?php echo $_SESSION['nome_aluno']?></h2>
                    </div><!--infoUser-->
                </div><!--headerCardUser-->
            </div><!--cardUser-->
        </div><!--row-->
    </div><!--wrap-->
</section><!--infoUser-->


<script src="<?php echo INCLUDE_PATH; ?>js/areaAluno.js"></script>
<?php }else{ 
    \Painel::redirect(INCLUDE_PATH);
} ?>

















