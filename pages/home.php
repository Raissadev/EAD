<section class="myApresentation">
    <div class="wrap heightComplet">
        <div class="row w85 center itemsFlex alignCenter heightComplet">
            <div class="title">
                <p>CREATIVE MIND, CREATIVE WORKS.</p>
                <h2 class="marginDownDefault">We are digital <br /> agency.</h2>
                <a class="button">Começar agora</a>
            </div><!--title-->
        </div><!--row-->
    </div><!--wrap-->
</section><!--myApresentation-->
<section class="boxesList marginDownBigger">
    <div class="wrap">
        <div class="row w100">
            <div class="boxes gridThree w80 center gridOneMobile">
                <div class="card marginSideSmallIn marginSideSmallInMobile">
                    <div class="cardHeader">
                        <i class="ri-mail-send-fill"></i>
                    </div><!--cardHeader-->
                    <div class="cardBody">
                        <h2>Future Concept.</h2>
                        <div class="divider">
                            <span></span>
                        </div><!--divider-->
                    </div><!--cardBody-->
                    <div class="cardFooter">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus.​</p>
                    </div><!--cardFooter-->
                </div><!--card-->
                <div class="card marginSideSmallIn active marginSideSmallInMobile">
                    <div class="cardHeader">
                        <i class="ri-chat-voice-fill"></i>
                    </div><!--cardHeader-->
                    <div class="cardBody">
                        <h2>Future Concept.</h2>
                        <div class="divider">
                            <span></span>
                        </div><!--divider-->
                    </div><!--cardBody-->
                    <div class="cardFooter">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus.​</p>
                    </div><!--cardFooter-->
                    <div class="dividerVertical">
                        <span></span>
                    </div><!--dividerVertical-->
                </div><!--card-->
                <div class="card marginSideSmallIn marginSideSmallInMobile">
                    <div class="cardHeader">
                        <i class="ri-stack-fill"></i>
                    </div><!--cardHeader-->
                    <div class="cardBody">
                        <h2>Future Concept.</h2>
                        <div class="divider">
                            <span></span>
                        </div><!--divider-->
                    </div><!--cardBody-->
                    <div class="cardFooter">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus.​</p>
                    </div><!--cardFooter-->
                </div><!--card-->
            </div><!--boxes-->
        </div><!--row-->
    </div><!--wrap-->
</section><!--boxesList-->

<section class="myApresentationServices sectionDefault" id="services">
    <div class="wrap w85 center">
        <div class="row marginDownDefault">
            <div class="title textCenterMobile">
                <div class="decoration itemsFlex alignCenter justCenterMobile">
                    <span class="line"></span><h3>Services.</h3>
                </div><!--decoration-->
                <p>Lorem ipsum dolor sit amet</p>
            </div><!--title-->
        </div><!--row-->
        <div class="row marginTopDefault">
            <div class="boxes gridThree marginDownSmall gridOneMobile">
                <div class="card cardTwo marginSideSmallIn marginSideSmallInMobile">
                    <div class="cardHeader">
                        <i class="ri-bar-chart-line"></i>
                    </div><!--cardHeader-->
                    <div class="cardBody marginDownSmall">
                        <h2>Future Concept.</h2>
                    </div><!--cardBody-->
                    <div class="cardFooter">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus.​</p>
                    </div><!--cardFooter-->
                </div><!--card-->
                <div class="card cardTwo marginSideSmallIn marginSideSmallInMobile">
                    <div class="cardHeader">
                        <i class="ri-pencil-line"></i>
                    </div><!--cardHeader-->
                    <div class="cardBody marginDownSmall">
                        <h2>Future Concept.</h2>
                    </div><!--cardBody-->
                    <div class="cardFooter">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus.​</p>
                    </div><!--cardFooter-->
                </div><!--card-->
                <div class="card cardTwo marginSideSmallIn active marginSideSmallInMobile">
                    <div class="cardHeader">
                        <i class="ri-user-heart-line"></i>
                    </div><!--cardHeader-->
                    <div class="cardBody marginDownSmall">
                        <h2>Future Concept.</h2>
                    </div><!--cardBody-->
                    <div class="cardFooter">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus.​</p>
                    </div><!--cardFooter-->
                </div><!--card-->
            </div><!--boxes-->
            <div class="boxes gridThree gridOneMobile">
                <div class="card cardTwo marginSideSmallIn marginSideSmallInMobile">
                    <div class="cardHeader">
                        <i class="ri-global-line"></i>
                    </div><!--cardHeader-->
                    <div class="cardBody marginDownSmall">
                        <h2>Future Concept.</h2>
                    </div><!--cardBody-->
                    <div class="cardFooter">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus.​</p>
                    </div><!--cardFooter-->
                </div><!--card-->
                <div class="card cardTwo marginSideSmallIn marginSideSmallInMobile">
                    <div class="cardHeader">
                        <i class="ri-bookmark-line"></i>
                    </div><!--cardHeader-->
                    <div class="cardBody marginDownSmall">
                        <h2>Future Concept.</h2>
                    </div><!--cardBody-->
                    <div class="cardFooter">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus.​</p>
                    </div><!--cardFooter-->
                </div><!--card-->
                <div class="card cardTwo marginSideSmallIn marginSideSmallInMobile">
                    <div class="cardHeader">
                        <i class="ri-medal-2-line"></i>
                    </div><!--cardHeader-->
                    <div class="cardBody marginDownSmall">
                        <h2>Future Concept.</h2>
                    </div><!--cardBody-->
                    <div class="cardFooter">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus.​</p>
                    </div><!--cardFooter-->
                </div><!--card-->
            </div><!--boxes-->
            <div class="dividerVertical">
                <span></span>
            </div><!--dividerVertical-->
        </div><!--row-->
    </div><!--wrap-->
</section><!--myApresentaionServices-->

<section class="myApresentationWorks sectionDefault marginTopBigger" id="works">
    <div class="wrap">
        <div class="row marginDownDefault">
            <div class="title w85 center textCenterMobile">
                <div class="decoration itemsFlex alignCenter justCenterMobile">
                    <span class="line"></span><h3>Works.</h3>
                </div><!--decoration-->
                <p>Lorem ipsum dolor sit amet</p>
            </div><!--title-->
        </div><!--row-->
        <div class="row marginTopDefault">
            <div class="listImgs itemsFlex">
                <figure>
                    <img src="<?php echo INCLUDE_PATH; ?>images/bgListTwo.jpg" />
                </figure>
                <figure>
                    <img src="<?php echo INCLUDE_PATH; ?>images/bgListOne.jpg" />
                </figure>
                <figure>
                    <img src="<?php echo INCLUDE_PATH; ?>images/bgListThree.jpg" />
                </figure>
                <figure>
                    <img src="<?php echo INCLUDE_PATH; ?>images/bgMain.jpg" />
                </figure>
                <figure>
                    <img src="<?php echo INCLUDE_PATH; ?>images/bgListOne.jpg" />
                </figure>
                <figure>
                    <img src="<?php echo INCLUDE_PATH; ?>images/bgListTwo.jpg" />
                </figure>
            </div><!--listImgs-->
            <div class="dividerVertical">
                <span></span>
            </div><!--dividerVertical-->
        </div><!--row-->
    </div><!--wrap-->
</section><!--myApresentationWorks-->


<section class="myApresentationAbout sectionDefault marginDownBigger marginTopMiddle" id="about">
    <div class="wrap">
        <div class="row marginDownDefault w85 center textCenterMobile">
            <div class="title">
                <div class="decoration itemsFlex alignCenter justCenterMobile">
                    <span class="line"></span><h3>About.</h3>
                </div><!--decoration-->
                <p>Lorem ipsum dolor sit amet</p>
            </div><!--title-->
        </div><!--row-->
        <div class="row marginTopDefault w85 center">
            <div class="listAbout gridTwo marginDownDefault gridOneMobile">
                <div class="col marginSideSmall marginDownSmallInMobile marginSideSmallInMobile">
                    <div class="cardAbout">
                        <figure class="itemsFlex alignEnd" style="background-image:url('<?php echo INCLUDE_PATH; ?>images/bgListOne.jpg');">
                            <div class="textInfo">    
                                <h4>Team Work</h4>
                                <p>Committed and creative</p>
                            </div><!--textInfo-->
                        </figure>
                    </div><!--cardAbout-->
                </div><!--col-->
                <div class="col marginSideSmall marginSideSmallInMobile">
                    <div class="cardAbout marginDownSmallInMobile">
                        <figure class="marginDownSmallInMobile itemsFlex alignEnd" style="background-image:url('<?php echo INCLUDE_PATH; ?>images/bgListTwo.jpg');">
                            <div class="textInfo">    
                                <h4>Team Work</h4>
                                <p>Committed and creative</p>
                            </div><!--textInfo-->
                        </figure>
                    </div><!--cardAbout-->
                    <div class="cardAbout marginDownSmallInMobile">
                        <figure class="itemsFlex alignEnd" style="background-image:url('<?php echo INCLUDE_PATH; ?>images/bgListThree.jpg');">
                            <div class="textInfo">    
                                <h4>Team Work</h4>
                                <p>Committed and creative</p>
                            </div><!--textInfo-->
                        </figure>
                    </div><!--cardAbout-->
                </div><!--col-->
            </div><!--listAbout-->
            <div class="infoAbout gridThree gridOneMobile">
                <div class="cardAbout cardInfo marginSideSmall itemsFlex alignCenter">
                    <div class="info textCenterMobile">
                        <h4>Who we are</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
                    </div><!--info-->
                </div><!--cardInfo-->
                <div class="cardAbout cardInfo marginSideSmall itemsFlex alignCenter">
                    <div class="info textCenterMobile">
                        <h4>Who we are</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
                    </div><!--info-->
                </div><!--cardInfo-->
                <div class="cardAbout cardInfo marginSideSmall itemsFlex alignCenter">
                    <div class="info textCenterMobile">
                        <h4>Who we are</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
                    </div><!--info-->
                </div><!--cardInfo-->
            </div><!--infoAbout-->
            <div class="dividerVertical">
                <span></span>
            </div><!--dividerVertical-->
        </div><!--row-->
    </div><!--wrap-->
</section><!--myApresentationWorks-->

<section class="myApresentationTestimonials sectionDefault">
    <div class="wrap">
        <div class="row marginDownDefault">
            <div class="title w85 center textCenterMobile">
                <div class="decoration itemsFlex alignCenter justCenterMobile">
                    <span class="line"></span><h3>Testimonials.</h3>
                </div><!--decoration-->
                <p>Lorem ipsum dolor sit amet</p>
            </div><!--title-->
        </div><!--row-->
        <div class="row marginTopDefault w95 center sliderElm">
            <a class="btnArrow arrowLeft"><i class="ri-arrow-left-s-line"></i></a>
            <div class="listTestimonials itemsFlex">
                <?php
                    for($i = 0; $i - 10; $i++){
                ?>
                <div class="card cardTestimonial">
                    <div class="bodyCard textCenter">
                        <i class="ri-double-quotes-r marginDownSmall"></i>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque consequat bibendum turpis sit amet pretium. Nunc ut dui ornare, vulputate augue sed, varius velit.</p>
                        <div class="ratings marginTopDefault">
                            <ul class="itemsFlex alignCenter justCenter">
                                <li><i class="ri-star-fill"></i></li>
                                <li><i class="ri-star-fill"></i></li>
                                <li><i class="ri-star-fill"></i></li>
                                <li><i class="ri-star-fill"></i></li>
                                <li><i class="ri-star-fill"></i></li>
                            </ul>
                        </div><!--ratings-->
                    </div><!--bodyCard-->
                    <div class="userInfo itemsFlex justCenter">
                        <div class="userWrap">
                            <figure class="userImg textCenter">
                                <img src="<?php echo INCLUDE_PATH; ?>images/myWallpaper.jpg" />
                            </figure>
                            <div class="textUser textCenter">
                                <h2>Jhon Doe</h2>
                                <p>Designer</p>
                            </div><!--textUser-->
                        </div><!--userWrap-->
                    </div><!--infoUser-->
                </div><!--card-->
                <?php } ?>
            </div><!--listTestimonials-->
            <a class="btnArrow arrowRight"><i class="ri-arrow-right-s-line"></i></a>
            <div class="dividerVertical marginTopBigger">
                <span></span>
            </div><!--dividerVertical-->
        </div><!--row-->
    </div><!--wrap-->
</section><!--myApresentationWorks-->

<section class="myApresentationBlog sectionDefault marginDownBigger" id="blog">
    <div class="wrap">
        <div class="row marginDownDefault w85 center">
            <div class="title textCenter">
                <h3>Latest News.</h3>
                <p>Lorem ipsum dolor sit amet</p>
            </div><!--title-->
        </div><!--row-->
        <div class="row marginTopDefault w95 center">
            <div class="listPosts gridThree gridOneMobile">
                <?php 
                    for($i = 0; $i < 3; $i++){
                ?>
                <div class="card cardTestimonial marginSideSmallIn marginSideSmallInMobile">
                    <figure class="imgPost">
                        <img src="<?php echo INCLUDE_PATH ?>images/bgMain.jpg" />
                    </figure><!--imgPost-->
                    <div class="bodyInfo">
                        <p>Could this VR sketching tool be coming of age for designers in the future? <br />
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas scelerisque ornare tincidunt....</p>
                        <div class="actionBtn marginTopDefault">
                            <a class="button">Ver mais</a>
                        </div><!--actionBtn-->
                    </div><!--bodyInfo-->
                </div><!--card-->
                <?php } ?>
            </div><!--listPosts-->
        </div><!--row-->
    </div><!--wrap-->
</section><!--myApresentationWorks-->

<section class="networks sectionDefault itemsFlex alignCenter justCenter" id="networks">
    <div class="wrap w95 center">
        <div class="dividerVertical marginTopBigger">
            <span></span>
        </div><!--dividerVertical-->
        <div class="row">
            <div class="socials">
                <ul class="itemsFlex alignCenter justCenter">
                    <li><a href="https://github.com/Raissadev"><i class="ri-github-fill"></i></a></li>
                    <li><a href="https://www.linkedin.com/in/raissa-dev-69986a214/"><i class="ri-linkedin-fill"></i></a></li>
                    <li><a href="https://www.instagram.com/raissa_dev/"><i class="ri-instagram-line"></i></a></li>
                    <li><a><i class="ri-twitter-fill"></i></a></li>
                </ul>
            </div><!--socials-->
            <div class="textAction title textCenter">
                <h3>Tem algum projeto em mente?</h3>
                <a class="button">Iniciar agora</a>
            </div><!--textAction-->
        </div><!--row-->
    </div><!--wrap-->
</section><!--networks-->
