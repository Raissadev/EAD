<?php
if(isset($_POST['acao'])){
    $login = $_POST['login'];
    $senha = $_POST['senha'];
    $sql = \MySql::conectar()->prepare("SELECT * FROM `tb_admin.alunos` WHERE email = ? AND senha = ?");
    $sql->execute(array($login,$senha));
    if($sql->rowCount() == 1){
        $info = $sql->fetch();
        $_SESSION['login_aluno'] = $login;
        $_SESSION['nome_aluno'] = $info['nome'];
        $_SESSION['id_aluno'] = $info['id'];
        $_SESSION['email_aluno'] = $info['email'];
        $_SESSION['senha_aluno'] = $info['senha'];


        \Painel::redirect(INCLUDE_PATH);
    }else{
        \Painel::redirect(INCLUDE_PATH);
    }
}
?>
<section class="boxLogin itemsFlex alignCenter justCenter">
    <div class="wrap">
        <div class="row">
            <div class="title textCenter marginDownDefault">
                <h3>Faça Login!</h3>
                <p>LOREM IPSUM DOLOR SIT AMET</p>
            </div><!--title-->
            <div class="cardLogin itemsFlex alignCenter justCenter">
                <form method="post" class="textCenter">
                    <input type="text" name="login" placeholder="Seu login..." />
                    <input type="password" name="senha" placeholder="Sua senha..." />
                    <input type="submit" name="acao" value="Efetuar login!" />
                </form>
            </div><!--cardLogin-->
        </div><!--row-->
    </div><!--wrap-->
</section><!--boxLogin-->