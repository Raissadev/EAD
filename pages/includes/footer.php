<footer>
    <div class="wrap w90 center">
        <div class="row gridTwo alignCenter">
            <div class="copy">
                <p>Todos os direitos reservados - <span class="logoMyName itemsInlineFlex alignCenter"><a>RaissaDev</a></span></p>
            </div><!--copy-->
            <div class="networks itemsFlex justEnd">
                <a href="https://github.com/Raissadev"><i class="ri-github-fill"></i></a>
                <a href="https://www.instagram.com/raissa_dev/"><i class="ri-instagram-line"></i></a>
                <a href="https://www.linkedin.com/in/raissa-dev-69986a214/"><i class="ri-linkedin-line"></i></a>
            </div><!--networks-->
        </div><!--row-->
    </div><!--wrap-->
</footer>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="<?php echo INCLUDE_PATH; ?>js/constants.js"></script>
<script src="<?php echo INCLUDE_PATH; ?>js/scripts.js"></script>
<script src="<?php echo INCLUDE_PATH; ?>js/myfunctions.js"></script>
</body>
</html>