<!DOCTYPE html>
<html>
<head>
	<title>EAD</title>
	<link href="<?php echo INCLUDE_PATH; ?>css/style.css" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500&family=Roboto:ital,wght@0,400;0,500;0,700;1,300;1,400&display=swap" rel="stylesheet">
	<link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="palavras-chave,do,meu,site">
	<meta name="description" content="Descrição do meu website">
	<link rel="icon" href="<?php echo INCLUDE_PATH; ?>favicon.ico" type="image/x-icon" />
	<meta charset="utf-8" />
</head>
<body>
<base base="<?php echo INCLUDE_PATH; ?> " />
<header>
	<div class="wrap">
		<div class="row gridTwo alignCenter w90 center">
			<div class="logoMyName itemsFlex alignCenter">
				<h2><a href="<?php echo INCLUDE_PATH; ?>"><span>RaissaDev</span></a></h2>
			</div><!--logoHeader-->
			<div class="hideDesktop menuForMobile alignCenter justEnd">
				<i class="ri-menu-line menuMobile"></i>
			</div><!---->
			<nav class="menuItems hideMobile">
				<ul class="itemsFlex alignCenter justEnd itemsBlockMobile">
					<li class="active"><a href="<?php echo INCLUDE_PATH ?>">Início</a></li>
					<li><a href="#works">Trabalhos</a></li>
					<li href=""><a href="#about">Sobre</a></li>
					<li href=""><a href="#blog">Blog</a></li>
					<?php
						if(isset($_SESSION['login_aluno'])){
							echo '<li href=""><a href="'.INCLUDE_PATH.'area_aluno/aula/1">Curso</a></li>';
						}else{
								echo '<li href=""><a href="'.INCLUDE_PATH.'login">Entrar</a></li>';
							}
						
					?>
					<li href=""><a class="button hideMobile" href="https://github.com/Raissadev">GitHub</a></li>
					<?php 
						if(!isset($_SESSION['login_aluno'])){
							echo '<li class="search hideMobile" href=""><a><i class="ri-search-line"></i></a></li>';
						}
					?>
					<?php 
						if(isset($_SESSION['login_aluno'])){
							echo '<li class="search hideMobile" href=""><a href="'.INCLUDE_PATH.'?deslogar"><i class="ri-door-open-line"></i></a></li>';
						}
					?>
				</ul>
			</nav><!--menuItems-->
		</div><!--row-->
	</div><!--wrap-->
</header>
