<section class="myApresentation">
	<div class="wrapper items-flex">
		<div class="boxMessage">
			<h2>Adicionar Aula</h2>
		</div><!--boxMessage-->
	</div><!--wrapper-->
</section><!--myApresentation-->

<section class="contentForm headerCardDefault formDefault">
	<div class="wrap">
		<div class="row">
			<div class="cardBox card">
				<div class="headerCard">
					<h2>Cadastrar Aula</h2>
				</div><!--headerCard-->
				<div class="bodyCard">
					<form method="post" enctype="multipart/form-data">
						<?php
							if(isset($_POST['acao'])){
								$nome = $_POST['nome'];
                                $modulo_id = $_POST['modulo_id'];
                                $link = $_POST['link_aula'];

                                if($nome == '' || $link == ''){
                                    Painel::alert('erro','Você precisa preencher todos os campos!');
                                }else{
                                    $sql = \MySql::conectar()->prepare("INSERT INTO `tb_admin.aulas` VALUES (null,?,?,?)");
                                    $sql->execute(array($nome,$modulo_id,$link));
                                    Painel::alert('sucesso','A aula foi cadastrada com sucesso!');
                                    Painel::redirect('');
                                }
                            }
						?>
						<div class="formGroup">
							<label>Nome da Aula:</label>
							<input type="text" name="nome">
						</div><!--formGroup-->
						<div class="formGroup">
							<label>Módulo id:</label>
							<select name="modulo_id" class="w100">
                                <?php
                                    $modulos = \MySql::conectar()->prepare("SELECT * FROM `tb_admin.modulos`");
                                    $modulos->execute();
                                    $modulos = $modulos->fetchAll();
                                    foreach($modulos as $key => $value){
                                ?>
                                    <option value="<?php echo $value['id']?>"><?php echo $value['nome']?></option>
                                <?php } ?>
                            </select>
						</div><!--formGroup-->
						<div class="formGroup">
							<label>Link da aula:</label>
							<input type="text" name="link_aula">
						</div><!--formGroup-->
						<div class="formGroup">
							<input type="submit" name="acao" value="Cadastrar Aula!">
						</div><!--formGroup-->
					</form>
				</div><!--bodyCard-->
			</div><!--boxCard-->
		</div><!--row-->
	</div><!--wrap-->
</section><!--contentForm-->