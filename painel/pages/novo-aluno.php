<section class="myApresentation">
	<div class="wrapper items-flex">
		<div class="boxMessage">
			<h2>Adicionar Aluno</h2>
		</div><!--boxMessage-->
	</div><!--wrapper-->
</section><!--myApresentation-->

<section class="contentForm headerCardDefault formDefault">
	<div class="wrap">
		<div class="row">
			<div class="cardBox card">
				<div class="headerCard">
					<h2>Cadastrar Aluno</h2>
				</div><!--headerCard-->
				<div class="bodyCard">
					<form method="post" enctype="multipart/form-data">
						<?php
							if(isset($_POST['acao'])){
								$nome = $_POST['nome'];
                                $email = $_POST['email'];
                                $senha = $_POST['senha'];

                                if($nome == '' || $email == '' || $senha == ''){
                                    Painel::alert('erro','Você precisa preencher os campos!');
                                }else{
                                    $sql = \MySql::conectar()->prepare("INSERT INTO `tb_admin.alunos` VALUES (null,?,?,?)");
                                    $sql->execute(array($nome,$email,$senha));
                                    Painel::alert('sucesso','O aluno foi cadastrado com sucesso!');
                                    Painel::redirect('');
                                }
                            }
						?>
						<div class="formGroup">
							<label>Nome do aluno:</label>
							<input type="text" name="nome">
						</div><!--formGroup-->
						<div class="formGroup">
							<label>Senha:</label>
							<input type="password" name="senha">
						</div><!--formGroup-->
						<div class="formGroup">
							<label>Email:</label>
							<input type="email" name="email">
						</div><!--formGroup-->
						<div class="formGroup">
							<input type="submit" name="acao" value="Cadastrar Aluno!">
						</div><!--formGroup-->
					</form>
				</div><!--bodyCard-->
			</div><!--boxCard-->
		</div><!--row-->
	</div><!--wrap-->
</section><!--contentForm-->