<section class="myApresentation">
	<div class="wrapper items-flex">
		<div class="boxMessage">
			<h2>Adicionar Módulo</h2>
		</div><!--boxMessage-->
	</div><!--wrapper-->
</section><!--myApresentation-->

<section class="contentForm headerCardDefault formDefault">
	<div class="wrap">
		<div class="row">
			<div class="cardBox card">
				<div class="headerCard">
					<h2>Cadastrar Módulo</h2>
				</div><!--headerCard-->
				<div class="bodyCard">
					<form method="post" enctype="multipart/form-data">
						<?php
							if(isset($_POST['acao'])){
								$nome = $_POST['nome'];

                                if($nome == ''){
                                    Painel::alert('erro','Você precisa preencher os campos!');
                                }else{
                                    $sql = \MySql::conectar()->prepare("INSERT INTO `tb_admin.modulos` VALUES (null,?)");
                                    $sql->execute(array($nome));
                                    Painel::alert('sucesso','O módulo foi cadastrado com sucesso!');
                                    Painel::redirect('');
                                }
                            }
						?>
						<div class="formGroup">
							<label>Nome do módulo:</label>
							<input type="text" name="nome">
						</div><!--formGroup-->
						<div class="formGroup">
							<input type="submit" name="acao" value="Cadastrar Módulo!">
						</div><!--formGroup-->
					</form>
				</div><!--bodyCard-->
			</div><!--boxCard-->
		</div><!--row-->
	</div><!--wrap-->
</section><!--contentForm-->