# EAD
<h2>Sobre o Projeto</h2>
<p>Portal EAD conectado com o dashboard, no dashboard possui algumas funções como: adicionar o aluno, criar um novo módulo, criar uma nova aula...</p>

<br />
<h3>Front-end:</h3>
<ul>
  <li>HTML</li>
  <li>CSS</li>
  <li>JS</li>
</ul>
<h3>Back-end:</h3>
<ul>
  <li>PHP</li>
  <li>Database: MySql</li>
</ul>
<h3>Stacks:</h3>
<ul>
  <li>jQuery</li>
</ul>

![EADGif](https://user-images.githubusercontent.com/82960240/138716911-852dc8c8-dd41-43e5-be5a-5eaf92d7e5a8.gif)

<hr />
<h3>Autor</h3>
<h4>Raissa Arcaro Daros</h4>
<div style="display: inline_block;"><br>
   
[![Blog](https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white)](https://www.instagram.com/raissa_dev/)
[![Blog](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/raissa-dev-69986a214/)
[![Blog](https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white)](https://github.com/Raissadev/)  
   
</div>
