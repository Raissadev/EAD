<?php 
include('config.php'); 
Site::updateUsuarioOnline();
Site::contador(); 

$homeController = new controller\homeController();
$aulaController = new controller\aulaController();

Router::get('/',function() use ($homeController){
	$homeController->home();
});
Router::get('/login',function() use ($homeController){
	$homeController->login();
});
Router::get('/area_aluno/aula/?',function($arg) use ($aulaController){
	$aulaController->index($arg);
});


?>