 var toggleMenu = document.querySelectorAll('.menuMobile'),
    asideMobile = document.querySelector('header nav.menuItems');

for(var i = 0; i < toggleMenu.length; i++){
    toggleMenu[i].addEventListener('click', menuAction);
}

function menuAction(){
    if(asideMobile.classList.contains('hideMobile')){
        asideMobile.classList.remove('hideMobile');
    }else{
        asideMobile.classList.add('hideMobile')
    }
}

const sliderElm = document.querySelector(".listTestimonials");
const btnLeft = document.querySelector(".arrowLeft");
const btnRight = document.querySelector(".arrowRight");

const numberSliderBoxs = sliderElm.children.length;
let idxCurrentSlide = 0;

// Functions:
function moveSlider() {
  let leftMargin = (sliderElm.clientWidth / numberSliderBoxs) * idxCurrentSlide;
  sliderElm.style.marginLeft = -leftMargin + "px";
  console.log(sliderElm.clientWidth, leftMargin);
}
function moveLeft() {
  if (idxCurrentSlide === 0) idxCurrentSlide = numberSliderBoxs - 1;
  else idxCurrentSlide--;

  moveSlider();
}
function moveRight() {
  if (idxCurrentSlide === numberSliderBoxs - 1) idxCurrentSlide = 0;
  else idxCurrentSlide++;

  moveSlider();
}

// Event Listeners:
btnLeft.addEventListener("click", moveLeft);
btnRight.addEventListener("click", moveRight);
window.addEventListener("resize", moveSlider);



$('nav.menuItems a[href^="#"]').on('click', function(e) {
	e.preventDefault();
	var id = $(this).attr('href'),
			targetOffset = $(id).offset().top;
			
	$('html, body').animate({ 
		scrollTop: targetOffset - 50
	}, 500);
});
$('nav.menuItems li').click(function() {
    $(this).siblings('li').removeClass('active');
    $(this).addClass('active');
});

/* */


